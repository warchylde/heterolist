import Item from './Item'

export default class News extends Item {
  // static get type () {
  //   return 'news'
  // }

  constructor (options = {}) {
    super(options)

    const { title, content, familiarized = false } = options

    Object.assign(this, {
      title,
      content,
      familiarized
    })
  }

  familiarize () {
    this.familiarized = true
  }
}
