import Item from './Item'

export default class Transaction extends Item {
  // static get type () {
  //   return 'transaction'
  // }

  constructor (options = {}) {
    super(options)

    const {
      sum,
      currency,
      source,
      description,
      direction
    } = options

    Object.assign(this, {
      sum,
      currency,
      source,
      description,
      direction
    })
  }

  get formattedSum () {
    return [
      Transaction.DIRECTIONS[this.direction],
      this.currency,
      this.sum
    ].join('')
  }

  static get DIRECTION_DEBIT () {
    return 'debit'
  }

  static get DIRECTION_CREDIT () {
    return 'credit'
  }

  static get DIRECTIONS () {
    return {
      [Transaction.DIRECTION_DEBIT]: '+',
      [Transaction.DIRECTION_CREDIT]: '-'
    }
  }
}
