export const FIELD_ID = 'id'
export const FIELD_DATE = 'date'
export const FIELD_TYPE = 'type'

export default class Item {
  static get type () {
    return this.name
  }

  constructor ({ id, type, date } = {}) {
    Object.assign(this, {
      id,
      date
    })
  }

  get type () {
    return this.constructor.type
  }

  remove () {
    this.remove = true
  }

  clone () {
    return new this.constructor(this)
  }
}
