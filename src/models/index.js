import News from './News'
import Transaction from './Transaction'

const models = [
  News,
  Transaction
]

export const modelIndex = models.reduce((res, constructor) => Object.assign(res, {
  [constructor.type]: constructor
}), {})

export default models
