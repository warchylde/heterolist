import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import Edit from './views/Edit.vue'
import Open from './views/Open.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/add-item/:type',
      name: 'edit',
      component: Edit,
      props: true
    },
    {
      path: '/open-item/:id',
      name: 'open',
      component: Open,
      props: true
    }
  ]
})
