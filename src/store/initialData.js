import NewsModel from '../models/News'
import TransactionModel from '../models/Transaction'

export default [
  new NewsModel({
    id: 1,
    date: new Date('2/1/2019 12:00'),
    title: 'The first news',
    content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
  }),
  new TransactionModel({
    id: 2,
    sum: 1000,
    currency: '$',
    source: 'Some One',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
    direction: TransactionModel.DIRECTION_CREDIT,
    date: new Date('2/1/2019 12:30')
  }),
  new TransactionModel({
    id: 3,
    sum: 500,
    currency: '$',
    source: 'Some Other',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
    direction: TransactionModel.DIRECTION_DEBIT,
    date: new Date('2/1/2019 12:31')
  })
]
