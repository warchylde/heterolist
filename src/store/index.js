import Vue from 'vue'
import Vuex from 'vuex'
import createLogger from 'vuex/dist/logger'
import data from './initialData'
import findIndex from 'lodash/findIndex'
import maxBy from 'lodash/maxBy'
import orderBy from 'lodash/orderBy'
import { FIELD_ID } from '../models/Item'

Vue.use(Vuex)

const debug = true

const INITIAL_ID = 0

const state = {
  items: data || [],
  maxId: data.length === 0
    ? INITIAL_ID
    : maxBy(data, FIELD_ID)[FIELD_ID]
}
const getters = {
  order,
  extract
}
const actions = {}
const mutations = {
  store,
  push,
  remove
}

export default new Vuex.Store({
  state,
  getters,
  actions,
  mutations,
  strict: debug,
  plugins: debug
    ? [createLogger()]
    : []
})

function store (state, item) {
  const { [FIELD_ID]: id } = item
  const { items } = state

  if (id === undefined) {
    push(state, item)
  } else {
    const pos = findIndex(items, { [FIELD_ID]: id })

    if (pos !== -1) {
      state.items = items.slice(0, pos).concat(item, items.slice(pos + 1))
    } else {
      push(state, item)
    }
  }
}

function push (state, item) {
  item.id = ++state.maxId
  state.items = state.items.concat(item)
}

function order (state) {
  return function ({ fields = [], directs = [] } = {}) {
    return fields.length
      ? orderBy(state.items, fields, directs)
      : state.items
  }
}

function extract (state) {
  return id => {
    const item = state.items.find(item => item.id === id)
    return item && item.clone()
  }
}

function remove (state, { [FIELD_ID]: id }) {
  const { items } = state
  const pos = findIndex(items, { [FIELD_ID]: id })

  if (pos !== -1) {
    state.items = items.slice(0, pos).concat(items.slice(pos + 1))
  }
}
